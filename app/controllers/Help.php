<?php

    include(APPROOT . '/helper/helperfunctions.php');

    class Help extends Controller {

        public function __construct() {
            $this->Arrangement = $this->model('Arrangement');

        }

        
        public function index() {

            $Arrangement = $this->Arrangement->getAllArrangement();
            $title = $this->Arrangement->title();

            $data = [
                'title' => $title,
                'Arrangement' => $Arrangement
            ];

            $this->view('pages/help', $data);

        }
        public function addArrangement() {

          $msg =  'Name: ' .$_POST['name'] ."\n".
                  'Email: ' .$_POST['email'] ."\n".
                  'Category: ' .$_POST['category'] ."\n".
                  'Comment: ' ."\n" .$_POST['comment'];
            mail('nqt596927536@me.com','Arrangement Form', $msg);

            if(!empty($_POST['name']) && !empty($_POST['email']) && !empty($_POST['category']) && !empty($_POST['comment'])){
                if($this->Arrangement->addArrangement($_POST['name'], $_POST['email'], $_POST['category'], $_POST['comment'])) {

                    $data = [
                        'title' => "Arrangement made successfully!",
                        'result' => true
                    ];
                }
            } else {
                $data = [
                    'title' => "There is some error to contact us, please try again later!",
                    'result' => false
                ];
            }

            $this->view('pages/help', $data);

        }

    }

?>