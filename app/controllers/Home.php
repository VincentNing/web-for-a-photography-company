<?php

    include(APPROOT . '/helper/helperfunctions.php');
    
    class Home extends Controller {

        public function __construct() {
            $this->ContactUs = $this->model('ContactUs');
            
        }

        
        public function index() {

            $ContactUs = $this->ContactUs->getAllContactUs();
            $title = $this->ContactUs->title();

            $data = [
                'title' => $title,
                'ContactUs' => $ContactUs
            ];

            $this->view('pages/home', $data);

        }

        public function addContactUs() {

            $msg =  'Name: ' .mysqli_real_escape_string($_POST['name']) ."\n".
                    'Email: ' .mysqli_real_escape_string($_POST['email']) ."\n".
                    'Category: ' .mysqli_real_escape_string($_POST['category']) ."\n".
                    'Comment: ' ."\n" .mysqli_real_escape_string($_POST['comment']);
                    mail('nqt596927536@me.com','Arrangement Form', $msg);

            if(!empty($_POST['name']) && !empty($_POST['email']) && !empty($_POST['category']) && !empty($_POST['comment'])){
                if($this->ContactUs->addContactUs($_POST['name'], $_POST['email'], $_POST['category'], $_POST['comment'])) {
                    $data = [
                        'title' => "Contacted successfully, we will get back to you as soon as possible"
                    ];
                }
            } else {
                $data = [
                    'title' => "There is some error to contact us, please try again later!"
                ];
            }

            $this->view('pages/home', $data);

        }

    }

?>