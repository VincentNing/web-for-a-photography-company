<?php

    class Arrangement {

        private $db;

        public function __construct() {
            $this->db = new Database;
        }

        public function title() {
            return "Show All Arrangement";
        }

        public function getAllArrangement() {
            $this->db->query('SELECT * FROM tbl_arrangement');
            return $this->db->resultSet();
        }

        public function getSingleArrangement($id) {
            $this->db->query('SELECT * FROM tbl_arrangement WHERE ID = :id');
            $this->db->bind(":id", $id);
            return $this->db->resultSet();
        }

        public function addArrangement($name, $email, $category, $comment) {

            $this->db->query('INSERT INTO  tbl_arrangement (Name, Mail, Category, MESSAGE) VALUES (:name, :email, :category, :comment)');


            $this->db->bind(':name', $name);
            $this->db->bind(':email', $email);
            $this->db->bind(':category', $category);
            $this->db->bind(':comment', $comment);

            if($this->db->execute()) {
                return true;
            } else {
                return false;
            }

        }

    }

?>