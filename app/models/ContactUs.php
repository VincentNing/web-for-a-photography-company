<?php

    class ContactUs {

        private $db;

        public function __construct() {
            $this->db = new Database;
        }

        public function title() {
            return "Show All ContactUs";
        }

        public function getAllContactUs() {
            $this->db->query('SELECT * FROM tbl_contactUs');
            return $this->db->resultSet();
        }

        public function getSingleContactUs($id) {
            $this->db->query('SELECT * FROM tbl_contactUs WHERE ID = :id');
            $this->db->bind(":id", $id);
            return $this->db->resultSet();
        }

        public function addContactUs($name, $email, $category, $comment) {

            $this->db->query('INSERT INTO  tbl_contactUs (Name, Mail, Category, Message) VALUES (:name, :email, :category, :comment)');


            $this->db->bind(':name', $name);
            $this->db->bind(':email', $email);
            $this->db->bind(':category', $category);
            $this->db->bind(':comment', $comment);

            if($this->db->execute()) {
                return true;
            } else {
                return false;
            }

        }

    }

?>