
  <div class="bgded overlay" style="background-image:url('<?php echo URLROOT?>public/images/placeholder.png');">
    <footer id="footer" class="hoc clear center">
      <h3 class="heading uppercase">Moment Pauser</h3>
      <ul class="faico clear">
        <li>
          <a class="faicon-facebook" href="http://www.facebook.com">
            <i class="fab fa-facebook-f"></i>
          </a>
        </li>
        <li>
          <a class="faicon-twitter" href="http://www.twitter.com">
            <i class="fab fa-twitter"></i>
          </a>
        </li>
        <li>
          <a class="faicon-dribble" href="http://www.dribbble.com">
            <i class="fab fa-dribbble"></i>
          </a>
        </li>
        <li>
          <a class="faicon-linkedin" href="http://nz.linkedin.com">
            <i class="fab fa-linkedin-in"></i>
          </a>
        </li>
        <li>
          <a class="faicon-google-plus" href="http://plus.google.com">
            <i class="fab fa-google-plus-g"></i>
          </a>
        </li>
        <li>
          <a class="faicon-vk" href="http://www.vk.com">
            <i class="fab fa-vk"></i>
          </a>
        </li>
      </ul>
    </footer>
    <div id="copyright" class="hoc clear center">
      <p>Copyright &copy; 2018 - All Rights Reserved -
        <a href="#">Moment Pauser</a>
      </p>
    </div>
  </div>
</body>
<!-- <script src="/css/bootstrap/js/bootstrap.min.js"></script> -->
<script src="/scripts/script.js"></script>


<script defer src="https://use.fontawesome.com/releases/v5.0.9/js/all.js" integrity="sha384-8iPTk2s/jMVj81dnzb/iFR2sdA7u06vHJyyLlAd4snFpCl/SnyUjRrbdJsw1pGIl" crossorigin="anonymous"></script>
<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script>
  function myFunction(x) {
    x.classList.toggle("change");
  }

  document.getElementById("showTop").addEventListener("click", function () {
    var display = document.getElementById("menu-main").style.display;
    document.getElementById("menu-main").style.display = (display == "none") ? "block" : "none";
  }, false);

</script>

</html>