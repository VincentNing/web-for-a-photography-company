<!DOCTYPE html>

<html>

<head>
  <title><?php echo SITENAME ?></title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <link rel="stylesheet" href="/css/framework.css">

  <link href="/css/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css" media="all">
  <link href="/css/layout.css" rel="stylesheet" type="text/css" media="all">
</head>

<body id="top">
  <a href="https://www.accuweather.com/en/nz/tauranga/246959/weather-forecast/246959" class="aw-widget-legal">
    <!--
By accessing and/or using this code snippet, you agree to AccuWeather’s terms and conditions (in English) which can be found at https://www.accuweather.com/en/free-weather-widgets/terms and AccuWeather’s Privacy Statement (in English) which can be found at https://www.accuweather.com/en/privacy.
-->
  </a>
  <div id="awcc1522988213910" class="aw-widget-current" data-locationkey="" data-unit="c" data-language="en-us" data-useip="true"
    data-uid="awcc1522988213910"></div>
  <script type="text/javascript" src="https://oap.accuweather.com/launch.js"></script>

  <div class="bgded overlay" style="background-image:url('/images/placeholder.png');">
    <div class="wrapper">
      <header id="header" class="hoc clear">
        <div id="logo">
          <h1>
            <a href="../index.php">Moment Pauser</a>
          </h1>
          <p>photographic company</p>
        </div>
        <nav id="site-nav">
          <button id="showTop" class="active"></button>
          <ul id="menu-main">
            <li id="menu-item-341">
              <a href="/Home">
                <span>Home</span>
              </a>
            </li>
            <li id="menu-item-342">
              <a href="/Gallery">
                <span>Gallery</span>
              </a>
            </li>
            <li id="menu-item-448">
              <a href="/Help">
                <span>Help</span>
              </a>
            </li>

          </ul>
        </nav>
        <nav id="mainav" class="clear">
          <ul class="clear">
            