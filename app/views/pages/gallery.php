<?php include(APPROOT . "/views/includes/header.php"); ?>
            <li>
              <a href="/Home">Home</a>
            </li>
            <li class="active">
              <a href="/Gallery">Gallery</a>
            </li>
            <li>
              <a href="/Help">Help</a>
            </li>

          </ul>
        </nav>
      </header>
    </div>
  </div>

  <div class="wrapper row3">
    <main class="hoc container clear">

      <div class="content">
        <div id="gallery">
          <figure>
            <header class="heading">Gallery Title Goes Here</header>
            <ul class="nospace clear">
              <li class="one_quarter changingmedia firstpic">
                <a href="#">
                  <img src="images/placeholder.png" alt="">
                </a>
              </li>
              <li class="one_quarter changingmedia">
                <a href="#">
                  <img src="images/placeholder.png" alt="">
                </a>
              </li>
              <li class="one_quarter changingmedia">
                <a href="#">
                  <img src="images/placeholder.png" alt="">
                </a>
              </li>
              <li class="one_quarter changingmedia">
                <a href="#">
                  <img src="images/placeholder.png" alt="">
                </a>
              </li>
              <li class="one_quarter changingmedia firstpic">
                <a href="#">
                  <img src="images/placeholder.png" alt="">
                </a>
              </li>
              <li class="one_quarter changingmedia">
                <a href="#">
                  <img src="images/placeholder.png" alt="">
                </a>
              </li>
              <li class="one_quarter changingmedia">
                <a href="#">
                  <img src="images/placeholder.png" alt="">
                </a>
              </li>
              <li class="one_quarter changingmedia">
                <a href="#">
                  <img src="images/placeholder.png" alt="">
                </a>
              </li>
              <li class="one_quarter changingmedia firstpic">
                <a href="#">
                  <img src="images/placeholder.png" alt="">
                </a>
              </li>
              <li class="one_quarter changingmedia">
                <a href="#">
                  <img src="images/placeholder.png" alt="">
                </a>
              </li>
              <li class="one_quarter changingmedia">
                <a href="#">
                  <img src="images/placeholder.png" alt="">
                </a>
              </li>
              <li class="one_quarter changingmedia">
                <a href="#">
                  <img src="images/placeholder.png" alt="">
                </a>
              </li>
            </ul>
            <figcaption>Gallery Description Goes Here</figcaption>
          </figure>
        </div>
        <nav class="pagination">
          <ul>
            <li>
              <a href="#">&laquo; Previous</a>
            </li>
            <li>
              <a href="#">1</a>
            </li>

            <li>
              <strong>&hellip;</strong>
            </li>
            <li>
              <a href="#">6</a>
            </li>
            <li class="current">
              <strong>7</strong>
            </li>
            <li>
              <a href="#">8</a>
            </li>

            <li>
              <strong>&hellip;</strong>
            </li>

            <li>
              <a href="#">15</a>
            </li>
            <li>
              <a href="#">Next &raquo;</a>
            </li>
          </ul>
        </nav>
      </div>

      <div class="clear"></div>
    </main>
  </div>

<?php include(APPROOT . "/views/includes/footer.php"); ?>