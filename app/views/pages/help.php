<?php include(APPROOT . "/views/includes/header.php"); ?>
            <li>
              <a href="/Home">Home</a>
            </li>
            <li>
              <a href="/gallery">Gallery</a>
            </li>
            <li class="active">
              <a href="/help">Help</a>
            </li>
          </ul>
        </nav>
      </header>
    </div>
  </div>

  <div class="wrapper row3 ">
    <main class="hoc container clear">

      <div class="content hoc">

        <h1>Some Paragraphs</h1>
        <img class="imgr borderedbox inspace-5 insertedPic" src="/images/placeholder.png" alt="">
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris eu enim egestas est iaculis luctus.
          <a href="#">Useful Link</a> Ut sed vestibulum nulla, in blandit sem. Curabitur luctus facilisis velit quis ultrices. Suspendisse
          gravida lectus ac turpis tristique imperdiet. Vestibulum condimentum porttitor feugiat. Sed quis massa augue. Maecenas
          eget risus eu metus vehicula laoreet eu at elit.
          <a href="#">Useful Link</a> Fusce vel quam a lorem tincidunt pretium. Proin fermentum facilisis nulla a tincidunt. Class aptent
          taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Morbi sit amet nulla tortor.
          <a
            href="#">Useful Link</a> Ut hendrerit orci id sem rhoncus semper. Vivamus ac ex in nibh euismod porttitor sit amet id lorem.
            Integer lacinia massa eget pretium pulvinar. Nam at nunc eros.</p>
        <img class="imgl borderedbox inspace-5 insertedPic" src="/images/placeholder.png" alt="">
        <p>Sed vel magna et leo laoreet elementum et vel nisl. Ut porta quam eget dictum ullamcorper.
          <a href="#">Useful Link</a> Nam ut tellus convallis, elementum felis vitae, semper ligula. Etiam mattis lacus facilisis lorem
          pulvinar varius. Nunc mi metus, luctus dapibus tortor in, vestibulum dapibus arcu. Quisque vehicula urna vitae
          viverra aliquam.
          <a href="#">Useful Link</a> Proin convallis nulla ullamcorper nisl convallis sagittis. Morbi luctus, metus nec pharetra lacinia,
          lectus risus rutrum tortor, sed vehicula ex dolor eu nunc.
          <a href="#">Useful Link</a> Cras odio lacus, pellentesque quis ligula eget, facilisis sagittis justo. Integer lobortis consectetur
          commodo. Suspendisse potenti. Proin et felis sed nibh faucibus euismod.</p>

      </div>
      <div class="clear"></div>
      <h3 class="padding-16 ">Our services</h3>
      <div class="row-padding" style="margin:0 -16px">
        <div class="gohalf x-margin-bottom">
          <ul class="x-ul  center x-opacity x-hover-opacity-off">
            <li class="x-dark-grey font-x3 padding-32">Basic</li>
            <li class="padding-16">Wedding</li>
            <li class="padding-16">Travel shots</li>
            <li class="padding-16">Portfolio</li>
            <li class="padding-16">Videos</li>
            <li class="padding-16">
              <h2>$ 30</h2>
              <span class="x-opacity">per hour</span>
            </li>
            <li class="x-light-grey x-padding-24">
              <button class="btn">chose</button>
            </li>
          </ul>
        </div>

        <div class="gohalf">
          <ul class="x-ul center x-opacity x-hover-opacity-off">
            <li class="dark-grey font-x3 padding-32">Pro</li>
            <li class="padding-16">Ultra Wedding</li>
            <li class="padding-16">Ultra Travel shots</li>
            <li class="padding-16">Ultra Portfolio</li>
            <li class="padding-16">ultra videos</li>
            <li class="padding-16">
              <h2>$ 55</h2>
              <span class="x-opacity">per hour</span>
            </li>
            <li class="padding-24">
              <button class="btn">chose</button>
            </li>
          </ul>
        </div>

      </div>
      <div class="row-padding hoc">
        <div class="comments">
          <h2>FAQs</h2>
          <ul>
            <li>
              <article>
                <header>
                  <figure class="avatar profile">
                    <img style="padding-left: 5px; padding-top: 5px;" src="/images/avatar.png" alt="">
                  </figure>
                  <address>
                    By
                    <a href="#">A Name</a>
                  </address>
                  <time datetime="2045-07-06T08:15+00:00">Friday, 6
                    <sup>th</sup> July 2045 @08:15:00</time>
                </header>
                <div class="comcont">
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris eu enim egestas est iaculis luctus. Ut
                    sed vestibulum nulla, in blandit sem. Curabitur luctus facilisis velit quis ultrices. Suspendisse gravida
                    lectus ac turpis tristique imperdiet.</p>
                </div>
                <div>
                  <strong>Solution</strong>
                  <div class="comcont">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris eu enim egestas est iaculis luctus. Ut
                      sed vestibulum nulla, in blandit sem. Curabitur luctus facilisis velit quis ultrices. Suspendisse gravida
                      lectus ac turpis tristique imperdiet.</p>
                  </div>
                </div>
              </article>
            </li>
            <li>
              <article>
                <header>
                  <figure class="avatar profile">
                    <img style="padding-left: 5px; padding-top: 5px;" src="/images/avatar.png" alt="">
                  </figure>
                  <address>
                    By
                    <a href="#">A Name</a>
                  </address>
                  <time datetime="2321-06-14T04:55+00:00">Wednesday, 14
                    <sup>th</sup> June 2312 @04:55:00</time>
                </header>
                <div class="comcont">
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris eu enim egestas est iaculis luctus. Ut
                    sed vestibulum nulla, in blandit sem. Curabitur luctus facilisis velit quis ultrices. Suspendisse gravida
                    lectus ac turpis tristique imperdiet.</p>
                </div>
                <div>
                  <strong>Solution</strong>
                  <div class="comcont">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris eu enim egestas est iaculis luctus. Ut
                      sed vestibulum nulla, in blandit sem. Curabitur luctus facilisis velit quis ultrices. Suspendisse gravida
                      lectus ac turpis tristique imperdiet.</p>
                  </div>
                </div>
              </article>
            </li>
            <li>
              <article>
                <header>
                  <figure class="avatar profile">
                    <img style="padding-left: 5px; padding-top: 5px;" src="/images/avatar.png" alt="">
                  </figure>
                  <address>
                    By
                    <a href="#">A Name</a>
                  </address>
                  <time datetime="2115-4-24T023:40+00:00">Monday, 24
                    <sup>th</sup> April 2115 @23:40:00</time>
                </header>
                <div class="comcont">
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris eu enim egestas est iaculis luctus. Ut
                    sed vestibulum nulla, in blandit sem. Curabitur luctus facilisis velit quis ultrices. Suspendisse gravida
                    lectus ac turpis tristique imperdiet.</p>
                </div>
                <div>
                  <strong>Solution</strong>
                  <div class="comcont">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris eu enim egestas est iaculis luctus. Ut
                      sed vestibulum nulla, in blandit sem. Curabitur luctus facilisis velit quis ultrices. Suspendisse gravida
                      lectus ac turpis tristique imperdiet.</p>
                  </div>
                </div>
              </article>
            </li>
          </ul>
        </div>
      </div>

      <h3 class="heading">Make A Arrangement</h3>
      <div class="comments">
      <form id="helpForm" name="help" class="needs-validation" method="post">
          <div class="one_third first">
            <label class="center" for="name">Name
              <span>*</span>
            </label>
            <input type="text" name="name" id="name" value="" size="22" required>
          </div>
          <div class="one_third">
            <label class="center" for="email">E-Mail
              <span>*</span>
            </label>
            <input type="email" name="email" id="email" value="" size="22" required>
          </div>
          <div class="one_third">
            <label class="center" for="category">Category</label>
            <input type="text" name="category" id="category" value="" size="22">
          </div>
          <div class="block clear">
            <label class="center" for="comment" >Message</label>
            <textarea name="comment" id="comment" cols="25" rows="10"></textarea>
          </div>
          <div class="center">
            <!-- <input type="submit" name="submit" value="Submit Form"> &nbsp; -->
            <button id="submit" name="submit" type="submit" class="submitButton">Submit Form</button>
            <button type="reset" name="reset" class="submitButton">Reset Form</button>
          </div>
        </form>
      </div>
      <div>
        <form class="center">

          <button  class="submitButton">chose a image</button>
          <button class="submitButton">Upload image</button>
        </form>
      </div>
      <div class="clear"></div>
    </main>
  </div>
  <div class="wrapper bgded overlay" style="background-image:url('/images/placeholder.png');">
    <article class="hoc container clear center">
      <h3 class="heading">Polices</h3>
      <p class="btmspace-50">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris eu enim egestas est iaculis luctus. </p>
      <a class="btn medium" href="Policy">Polices</a>
    </article>
  </div>

  <script>
<?php if(isset($data['result'])):?>
          alert('<?php echo $data['title'];?>');
<?php endif;?>
  </script>

<?php include(APPROOT . "/views/includes/footer.php"); ?>