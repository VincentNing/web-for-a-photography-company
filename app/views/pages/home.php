<?php include(APPROOT . "/views/includes/header.php"); ?>
            <li class="active">
              <a href="/Home">Home</a>
            </li>
            <li>
              <a href="/Gallery">Gallery</a>
            </li>
            <li>
              <a href="/Help">Help</a>
            </li>
          </ul>
        </nav>
      </header>
    </div>
    <div id="pageintro" class="hoc clear">
      <article>
        <em>Hello! My dear customer</em>
        <h2 class="heading">Welcome to Moment Pauser</h2>
        <p>It is nice to meet you here</p>
        <footer>
          <a class="btn" href="#">Start</a>
        </footer>
      </article>
    </div>
  </div>

  <div class="wrapper row3">
    <main class="hoc container clear">
      <ul class="nospace clear services">
        <li class="one_half first borderedbox">
          <div class="inspace-30">
            <h6 class="heading">About Us</h6>
            <p class="nospace">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur in orci viverra, malesuada ex quis, elementum
              orci. Donec pretium fermentum volutpat. Suspendisse venenatis sapien at augue vulputate, vel elementum massa
              rutrum. </p>
          </div>
        </li>
        <li class="one_quarter changingmedia">
          <a href="#">
            <figure>
              <img src="/images/placeholder.png" alt="">
              <figcaption>image1</figcaption>
            </figure>
          </a>
        </li>
        <li class="one_quarter changingmedia">
          <a href="#">
            <figure>
              <img src="/images/placeholder.png" alt="">
              <figcaption>image2</figcaption>
            </figure>
          </a>
        </li>
        <li class="one_quarter changingmedia firstpic ">
          <a href="#">
            <figure>
              <img src="/images/placeholder.png" alt="">
              <figcaption>image3</figcaption>
            </figure>
          </a>
        </li>
        <li class="one_quarter changingmedia ">
          <a href="#">
            <figure>
              <img src="/images/placeholder.png" alt="">
              <figcaption>image4</figcaption>
            </figure>
          </a>
        </li>
        <li class="one_quarter changingmedia">
          <a href="#">
            <figure>
              <img src="/images/placeholder.png" alt="">
              <figcaption>image5</figcaption>
            </figure>
          </a>
        </li>
        <li class="one_quarter changingmedia">
          <a href="#">
            <figure>
              <img src="/images/placeholder.png" alt="">
              <figcaption>image6</figcaption>
            </figure>
          </a>
        </li>
      </ul>

      <div class="clear"></div>
    </main>
  </div>
  <div class="wrapper bgded overlay" style="background-image:url('images/placeholder.png');">
    <article class="hoc container clear center">
      <h3 class="heading">Will be something here</h3>
      <p class="btmspace-50">Thank you very much for trying to know about us, we are a little bussiness doing photographic in Tauranga. We have the service at the open range of taking photos, such as portfolio taking, wedding shotting, professional liscence photos, editting photos or videos if needed and even can follow up a trip with the customer for a fancy traveling video can be arranged.
       </p>
      <a class="btn medium" href="#">Further Details</a>
    </article>
  </div>
  <div class="wrapper row3">
    <section class="hoc container clear">
      <div class="center btmspace-80">
        <h3 class="heading">Contact Us</h3>
        <p class="nospace ">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris eu enim egestas est iaculis luctus.</p>
        <div class="margin-top">
          <span class="font-x1">Phone</span>
          <br>
          <span class="font-x1">021-123321</span>
          <br>
        </div>
        <div class="margin-top">
          <span class="font-x1">E-mail</span>
          <br>
          <span class="font-x1">123321qweewq@qwe.com</span>
          <br>
        </div>

        <div class="margin-top">
          <span class="font-x1">Address</span>
          <br>
          <span class="font-x1">00,vincent St, City,Country</span>
          <br>
        </div>
         <div class="comments" style="margin-top:30px;">
        <form id="homeForm" name="home" class="needs-validation" action="" method="post">
          <div class="one_third first">
            <label class="center" for="name">Name
              <span>*</span>
            </label>
            <input type="text" name="name" id="name" value="" size="22" required>
          </div>
          <div class="one_third">
            <label class="center" for="email">E-Mail
              <span>*</span>
            </label>
            <input type="email" name="email" id="email" value="" size="22" required>
          </div>
          <div class="one_third">
            <label class="center" for="url">Category</label>
            <input type="text" name="category" id="category" value="" size="22">
          </div>
          <div class="block clear">
            <label class="center" for="comment">Message</label>
            <textarea name="comment" id="comment" cols="25" rows="10"></textarea>
          </div>
          <div class="center">
            <!-- <input type="submit" name="submit" value="Submit Form" > &nbsp; -->
            <button type="submit" class="submitButton">Submit Form</button>
            <button type="reset" name="reset" class="submitButton">Reset Form</button>
          </div>
        </form>

      </div>

      </div>
      <div id="map"></div>
      <script>
        function initMap() {
          var uluru = { lat: -37.685196, lng: 176.165765 };
          var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 18,
            center: uluru
          });
          var marker = new google.maps.Marker({
            position: uluru,
            map: map
          });
        }
      </script>
      <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAnv5k060UYf7fQ-Ullx93kZnL7WJfp_zA&callback=initMap">
      </script>
    </section>
  </div>
  <script>
<?php if(isset($data['result'])):?>
          alert('<?php echo $data['title'];?>');
<?php endif;?>
  </script>

  <?php include(APPROOT . "/views/includes/footer.php"); ?>