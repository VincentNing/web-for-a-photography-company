<?php include(APPROOT . "/views/includes/header.php"); ?>
            <li>
              <a href="/Home">Home</a>
            </li>
            <li>
              <a href="/Gallery">Gallery</a>
            </li>
            <li class="active">
              <a href="/Help">Help</a>
            </li>

          </ul>
        </nav>
      </header>
    </div>
  </div>
  <!-- End Top Background Image Wrapper -->
  <div class=container>
    <main class="row">
      <h1 class="center">Policy</h1>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus sit amet enim a augue mollis viverra. Vivamus dignissim
        tellus et ultrices pharetra. Nam et enim sit amet nisl malesuada interdum. In hac habitasse platea dictumst. Donec
        hendrerit, sem eu sagittis cursus, ligula nisl laoreet neque, in lobortis purus odio at mauris. Aenean eu metus enim.
        Integer laoreet sed diam vel dignissim. Interdum et malesuada fames ac ante ipsum primis in faucibus. Etiam venenatis,
        felis at gravida dapibus, ipsum velit ultrices nulla, eu faucibus sapien risus at nunc. Curabitur imperdiet lectus
        quis libero suscipit, nec dapibus enim laoreet. In eros orci, sollicitudin in nibh eu, blandit porttitor est.</p>
      <p>Donec eros lorem, dictum vel imperdiet sed, blandit sed sapien. Vivamus nisl libero, sodales id sagittis et, varius
        vel quam. Morbi eleifend dignissim eros quis placerat. Aenean ultricies metus turpis, at ultricies elit efficitur
        quis. Nam eu turpis lorem. Curabitur accumsan tincidunt ante a feugiat. Vestibulum augue velit, porttitor in placerat
        ut, euismod sit amet purus. Proin sit amet nulla sit amet lorem sollicitudin euismod. Quisque condimentum elementum
        orci, in posuere lorem faucibus a. Vivamus vitae vestibulum dolor, sit amet consectetur purus. Vestibulum vel mi
        mi. Proin feugiat, sapien quis malesuada gravida, ipsum est rhoncus augue, sed eleifend dolor metus eu diam. Aliquam
        sed sem sodales, semper odio eleifend, ornare felis. Cras tempus quam nec ligula gravida placerat sed a dolor.</p>
      <p>Morbi mattis, tellus id vulputate aliquet, ex nisl ornare justo, quis ultricies ligula nulla id massa. Mauris sed dolor
        at lectus auctor commodo vitae vel nulla. Quisque sodales purus hendrerit, egestas diam id, ullamcorper tortor. Donec
        sit amet turpis diam. Phasellus eget feugiat nisl. Nam scelerisque ornare ante, non volutpat ex eleifend ac. Aenean
        sollicitudin gravida ex et convallis.</p>
      <p>Nunc varius leo nec lobortis pharetra. Nullam pretium quam nunc, et ultrices dui laoreet ut. Cras nisl orci, interdum
        at rutrum sed, fringilla eu tortor. Suspendisse ornare orci non lectus accumsan, eget sagittis mi molestie. Donec
        et dolor cursus, sollicitudin eros ac, laoreet leo. Suspendisse tincidunt lectus a faucibus venenatis. Proin feugiat
        semper dui a posuere. Sed eleifend mollis sollicitudin. Mauris tristique euismod viverra. Vestibulum lorem ex, scelerisque
        quis magna ut, bibendum consequat sem. Fusce gravida orci a lectus posuere egestas.</p>
      <p>Etiam dui diam, auctor et porttitor vel, finibus quis orci. Fusce nunc odio, placerat et tortor in, venenatis ultricies
        ipsum. Nam porta sapien a enim interdum, ut euismod augue euismod. Phasellus rhoncus imperdiet mattis. Vestibulum
        ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed efficitur convallis felis a molestie.
        Sed in justo eu sapien fermentum semper.</p>
      <div class="clear"></div>
    </main>
  </div>

<?php include(APPROOT . "/views/includes/footer.php"); ?>